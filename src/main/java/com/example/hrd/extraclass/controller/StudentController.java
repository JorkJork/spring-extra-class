package com.example.hrd.extraclass.controller;


import com.example.hrd.extraclass.model.Students;
import com.example.hrd.extraclass.repository.StudentRepository;
import com.example.hrd.extraclass.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class StudentController {
    private StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("")
    public String ViewIndex(ModelMap modelMap){
        modelMap.addAttribute("studentsList",studentService.getAll());
        return "index";
    }
}
