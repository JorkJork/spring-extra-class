package com.example.hrd.extraclass.repository;

import com.example.hrd.extraclass.model.Students;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {
    List<Students> studentsList = new ArrayList<>();

    public StudentRepository(List<Students> studentsList) {
        for (int i=1;i<=10;i++){
            Students students = new Students();
            students.setId(i);
            students.setName("student "+i);
            students.setGender("Male");
            students.setAddress("Phnom Penh");
            studentsList.add(students);
        }
   }
    public List<Students> getAllStudent(){
        return studentsList;
}
}