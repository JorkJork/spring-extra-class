package com.example.hrd.extraclass.service.implement;

import com.example.hrd.extraclass.model.Students;
import com.example.hrd.extraclass.repository.StudentRepository;
import com.example.hrd.extraclass.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepository studentRepository;

    @Autowired
    public StudentServiceImp(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Students> getAll() {
        return studentRepository.getAllStudent();
    }
}
