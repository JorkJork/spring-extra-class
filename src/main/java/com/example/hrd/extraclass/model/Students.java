package com.example.hrd.extraclass.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Students {
    private int id;
    private String name;
    private String gender;
    private String address;


}
